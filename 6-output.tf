output "Instance-Public-Ip-Address" {
  value = aws_instance.test.public_ip
}

output "AMI-Id" {
  value = data.aws_ami.amazon-linux-2.id
}
variable "instance-type" {
  type = string
  default = "t2.micro"
}

variable "key-name" {
  type = string
  default = "apache_key"
}
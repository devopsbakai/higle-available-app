data "aws_ami" "amazon-linux-2" {
 most_recent = true
 owners = ["amazon"]

 filter {
   name   = "owner-alias"
   values = ["amazon"]
 }


 filter {
   name   = "name"
   values = ["amzn2-ami-hvm*"]
 }
}

resource "aws_default_subnet" "default_subnet" {
    availability_zone = "us-east-1a"
}

resource "aws_instance" "test" {
 ami                         = data.aws_ami.amazon-linux-2.id
 associate_public_ip_address = true
 instance_type               = var.instance-type
 key_name                    = var.key-name
 subnet_id                   = aws_default_subnet.default_subnet.id

}
## Terraform Class 2

#### You can change instance properties on 5-production.tfvars

#### Instructions to run a code
    terraform init 
    terraform plan
    terraform apply --var-file 5-production.tfvars --auto-approve
    

#### Don't forget to destroy your resources 😉
    terraform destroy --var-file 5-production.tfvars --auto-approve 
